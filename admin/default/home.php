<?php
/**
 *
 * Copyright (C) 2010  Arie Nugraha (dicarve@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    include_once '../../ucsysconfig.inc.php';
}
?>
<fieldset class="menuBox adminHome">
<div class="menuBoxInner">
	<div class="per_title">
    	<h2 style="margin-top:10px;"><?php echo $sysconf['server']['name']; ?><div class="pull-right"><small><?php echo $sysconf['server']['subname']; ?></small></div></h2>
	</div>
</div>
</fieldset>
<?php

$location = array();
$location[] = array('Perpustakaan A', -6.2297465,106.829518, '<b>Perpustakaan A</b>ini deksripsi berlaku kode html');
$location[] = array('Perpustakaan B', -6.2290465,106.820518, '<b>Perpustakaan B</b> ini deskripsi berlaku kode html');

// generate warning messages
$warnings = array();
// check GD extension
if (!extension_loaded('gd')) {
    $warnings[] = __('<strong>PHP GD</strong> extension is not installed. Please install it or application won\'t be able to create image thumbnail.');
}
// check mysqldump
if (!file_exists($sysconf['mysqldump'])) {
    $warnings[] = __('The PATH for <strong>mysqldump</strong> program is not right! Please check configuration file or you won\'t be able to do any database backups.');
}

// if there any warnings
if ($warnings) {
    echo '<div class="errorBox">';
    echo '<ul>';
    foreach ($warnings as $warning_msg) {
        echo '<li>'.$warning_msg.'</li>';
    }
    echo '</ul>';
    echo '</div>';
}

// admin page content
require LIB_DIR.'content.inc.php';
$content = new content();
$content_data = $content->get($dbs, 'adminhome');
if ($content_data) {
    //echo '<div class="contentDesc">'.$content_data['Content'].'</div>';
    unset($content_data);
}
?>
<div class="contentDesc">    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 s-dashboard">
              <div class="panel panel-info">
                <div class="panel-heading">
                  <h2 class="panel-title">Node Transaction Maps</h2>
                </div>
                <div class="panel-body">
                    <div class="well" id="map_canvas" style="min-height:500px !important;"></div>            
                </div>
                <div class="panel-footer">
                </div>
              </div>
            </div>
            <div class="col-lg-4 s-dashboard">
              <div class="panel panel-default s-dashboard">
                <div class="panel-heading">
                  <h2 class="panel-title">Record Transaction</h2>
                </div>
                <div class="panel-body">
                    <div class="s-chart">                        
                        <canvas id="radar-chartjs" width="175" height="175"></canvas>              
                    </div>
                </div>
                <div class="panel-footer">
                                     
                </div>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="s-widget-icon"><i class="fa fa-bookmark"></i></div>
                        <div class="s-widget-value" id="totalCollection">wait..</div>
                        <div class="s-widget-title">Total of Collections</div>              
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="s-widget-icon"><i class="fa fa-server"></i></div>
                        <div class="s-widget-value">7</div>
                        <div class="s-widget-title">Total of Client</div>                  
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="s-widget-icon"><i class="fa fa-exchange"></i></div>
                        <div class="s-widget-value">4</div>
                        <div class="s-widget-title">Online Client</div>                  
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-3 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="s-widget-icon"><i class="fa fa-retweet"></i></div>
                        <div class="s-widget-value">3</div>
                        <div class="s-widget-title" id="time">Offline Client</div>                  
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
</div>

   
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBZY7bxlwBh14vQZh3FogMPZcInxT6tgc" type="text/javascript"></script>
<script type="text/javascript"> 
  (function() {  
    var infowindow = null;     
        initialize(); 
  })();   
      function initialize() {   
          var sites = <?php echo json_encode($location); ?>  
          var centerMap = new google.maps.LatLng(-6.2297465,106.829518);         
          var myOptions = {  
            zoom: 5,  
            center: centerMap,  
            mapTypeId: google.maps.MapTypeId.ROADMAP 
            }    
          var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);    
             setMarkers(map, sites); 
             setAction(map); 
             infowindow = new google.maps.InfoWindow({  
                content: "loading..."  
              });  
          var bikeLayer = new google.maps.BicyclingLayer();  
          bikeLayer.setMap(map);       
      }   
      function setMarkers(map, markers) {  
        for (var i = 0; i < markers.length; i++) {  
         var sites = markers[i];  
         var siteLatLng = new google.maps.LatLng(sites[1], sites[2]);  
         var marker = new google.maps.Marker({  
            position: siteLatLng,  
            map: map,  
            title: sites[0],  
            zIndex: 1, 
            //icon: './themes/default/images/logo.png', 
            html: sites[3]     
            });     
          google.maps.event.addListener(marker, "mouseover", function () {             
            infowindow.setContent(this.html);  
            infowindow.open(map, this);  
          });  
        }  
      }  
      function setAction(map){           
          google.maps.event.addListener(map, "mouseover", function(event) {  
           var lat = event.latLng.lat();  
           var lng = event.latLng.lng();            
           google.maps.event.addListener(marker, "mouseover", function() {                  
              infowindow.setContent(this.html);  
              infowindow.open(map, this);  
            });             
        });  
      } 
    var auto_refresh = setInterval(
    function (){ 
        $.getJSON("<?php echo ADMIN_WEB_ROOT_DIR.'AJAX_notification.php';?>", function(data) {
            $.each(data, function(index, element) { 
                $("#totalCollection").html(element.node_id);
                $("#time").html(element.timer);
            });
        });
    }, 1000);


 </script>