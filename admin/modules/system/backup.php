<?php
/**
 *
 * Copyright (C) 2010  Arie Nugraha (dicarve@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* Backup Management section */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../ucsysconfig.inc.php';
// start the session
require UCS_BASE_DIR.'admin/default/session.inc.php';
require UCS_BASE_DIR.'admin/default/session_check.inc.php';

// privileges checking
$can_read = utility::havePrivilege('system', 'r');
$can_write = utility::havePrivilege('system', 'w');

if (!($can_read AND $can_write)) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to view this section').'</div>');
}
/* search form */
?>
<fieldset class="menuBox">
<div class="menuBoxInner backupIcon">
  <div class="per_title">
      <h2><?php echo __('Database Backup'); ?></h2>
  </div>
<?php
if (!file_exists($sysconf['mysqldump'])) {
    echo '<div class="alert alert-danger">'.__('The PATH for <strong>mysqldump</strong> program is not right! Please check configuration file or you won\'t be able to do any database backups.').'</div>';
}else{
?>
  <div class="sub_section">
    <div class="btn-group">
      <button onclick="$('#createBackup').submit()" class="notAJAX btn btn-success"><i class="glyphicon glyphicon-plus"></i>&nbsp;<?php echo __('Start New Backup'); ?></button>
    </div>    
    <form name="search" action="<?php echo MODULES_WEB_ROOT_DIR; ?>system/backup_proc.php" id="search" method="get" style="display: inline;"><?php echo __('Search'); ?> :
    <input type="text" name="keywords" size="30" />
    <input type="hidden" name="start"/>
    <input type="submit" id="doSearch" value="<?php echo __('Search'); ?>" class="btn btn-default" />
    </form>
    <form name="createBackup" id="createBackup" target="blindSubmit" action="<?php echo MODULES_WEB_ROOT_DIR; ?>system/backup_proc.php" method="post" style="display: inline; visibility: hidden;">
    </form>
  </div>
<?php } ?>
</div>
</fieldset>
<div id="backupStat">
<?php 
require 'backup_proc.php'; ?>
</div>
